'use strict';
var giniHealth = {
	debug: false,
	init: function() {
		// for mobile
		if(giniHealth.isMobile()) $('html').addClass('bp-touch');

		giniHealth.resizer(function() {
			giniHealth.resize();
		});

		$('.accordion').each(function() {
			var _accordion = this;

			$('> div', _accordion).each(function() {
				var _item = this;

				var _title = $('> h4', _item);
				var _content = $(_title).next();

				if(!_content[0]) return;
				$(_content).hide();

				$(_title).click(function(e) {
					e.preventDefault();

					if($(_title).hasClass('active')) {
						$(_title).removeClass('active');
						$(_content).slideUp(250);
					} else {
						$(_title).addClass('active');
						$(_content).slideDown(250);
					}
				});
			});
		});

		function deselect(e) {
			$('.pop').fadeToggle(function() {
				e.removeClass('selected');
			});
		}

		$('.contactpop').on('click', function() {
			if($(this).hasClass('selected')) {
				deselect($(this));               
			} else {
				$(this).addClass('selected');
				$('.pop').fadeToggle();
			}
			return false;
		});

		$('.close').on('click', function() {
			deselect($('.contactpop'));
			return false;
		});

		$.fn.fadeToggle = function(easing, callback) {
			return this.animate({ opacity: 'toggle'}, 'fast', easing, callback);
		};

		$('#datepicker').datepicker();

		// for popups
		giniHealth.popup.init();

		$('.btn-join-now').click(function(e) {
			e.preventDefault();

			giniHealth.popup.open({target: '#book-online-workshop'});
		});

		$.getJSON('assets/data/close-dates.json', function(data) {
			giniHealth.calendar.closeDates = data;
		});

		// for calendar
		$('[data-date-time-picker]').each(function() {
			var _options = {
				target: this
			}
			
			// custom attr
			giniHealth.getAttr({
				target: this,
				attr: 'dateTimePicker',
				after: function(attr) {
					// custom min date
					if(attr[0]) {
						_options.date = {};
						_options.date.min = parseInt(attr[0]);
					}

					// custom max date
					if(attr[1]) _options.date.max = parseInt(attr[1]);

					// custom opening time
					if(attr[2]) {
						_options.time = {};
						_options.time.selection = attr[2].split('|');
						_options.time.open = _options.time.selection[0];
						_options.time.close = _options.time.selection[_options.time.selection.length - 1];
					}
				}
			});

			new giniHealth.dateTimePicker(_options);
		});

		$(document).ready(function() {
			giniHealth.ready();
		});
	},
	ready: function() {
		// Animation
		AOS.init();
	},
	header: {
		target: document.querySelector('header'),
		height: function() {
			return giniHealth.header.target.offsetHeight;
		}
	},
	width: function() {
		return $(window).width();
	},
	height: function() {
		return $(window).height();
	},
	isMobile: function() {
		const isMobile = {
		    Android: function() {
		        return navigator.userAgent.match(/Android/i);
		    },
		    BlackBerry: function() {
		        return navigator.userAgent.match(/BlackBerry/i);
		    },
		    iOS: function() {
		        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		    },
		    Opera: function() {
		        return navigator.userAgent.match(/Opera Mini/i);
		    },
		    Windows: function() {
		        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
		    },
		    any: function() {
		        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		    }
		};
		if(isMobile.any) {
			return isMobile.any();
		}
	},
	getUrlVars: function() {
		let vars = {};
		const parts = window.location.href.replace(/[?&]+([^=&]+)=([^&#]*)/gi, function(m,key,value) {
			vars[key] = value;
		});
		return vars;
	},
	resize: function() {
		var _mainWrapper = document.querySelector('#main-wrapper');
		if(_mainWrapper) {
			$(_mainWrapper.firstElementChild).css({paddingTop: ''});
			var _paddingTop = parseInt($(_mainWrapper.firstElementChild).css('padding-top'));
			$(_mainWrapper.firstElementChild).css({paddingTop: _paddingTop + giniHealth.header.height()});
		}

		// for equal height
		$('.group-height').each(function() {
			if(giniHealth.width() >= 768) {
				giniHealth.equalize(this.querySelectorAll('.gh-md-1'));	
			} else {
				giniHealth.equalize(this.querySelectorAll('.gh-md-1'), 'clear');	
			}

			giniHealth.equalize(this.querySelectorAll('.gh1'));
			giniHealth.equalize(this.querySelectorAll('.gh2'));
			giniHealth.equalize(this.querySelectorAll('.gh3'));
			giniHealth.equalize(this.querySelectorAll('.gh4'));
			giniHealth.equalize(this.querySelectorAll('.gh5'));
		});
	},
	equalize: function(target, state) {
		for (let i = 0; i < target.length; i++) {
			target[i].style.minHeight = '';
		}

		if(state == 'clear') return;

		let _biggest = 0;
		for (let i = 0; i < target.length; i++ ){
			let element_height = target[i].offsetHeight;
			if(element_height > _biggest ) _biggest = element_height;
		}

		for (let i = 0; i < target.length; i++) {
			target[i].style.minHeight = _biggest + 'px';
		}
		
		return _biggest;
	},
	resizer: function(callback) {
		callback();

		var _resizeTimer = 0;
		window.addEventListener('resize', function() {
			callback();

			clearTimeout(_resizeTimer);
			_resizeTimer = this.setTimeout(function() {
				callback();
			}, 200);
		});

		window.addEventListener('load', function() {
			callback();
		});
	},
	dispatchEvent: function(elem, eventName) {
		let event;
		if (typeof(Event) === 'function') {
			event = new Event(eventName);
		}
		else {
			event = document.createEvent('Event');
			event.initEvent(eventName, true, true);
		}
		elem.dispatchEvent(event);
	},
	getAttr: function(opt) {
		const _attr = eval('opt.target.dataset.' + opt.attr);

		if(_attr) {
			if(opt.after) {
				if(opt.array) {
					opt.after(JSON.parse(_attr));
				} else {
					opt.after(_attr.split(','));
				}
			}
		}
	},
	popup: {
		init: function() {
			$('[data-popup]').each(function() {
				new giniHealth.popup.custom({target: this});
			});

			var _hash = window.location.hash;
			if(_hash) {
				_hash = _hash.split('?');

				var _autoPopup = document.querySelector(_hash[0]);
				if(!_autoPopup) return;

				if($(_autoPopup).hasClass('popup-content')) {
					setTimeout(function() {
						new giniHealth.popup.open({target: _hash[0]});
					}, 500);
				}
			}
		},
		popups: [],
		custom: function(opt) {
			var _popup = this;

			_popup.target = '';

			_popup.init = function() {
				_popup.target = opt.target;

				$(_popup.target).click(function(e) {
					e.preventDefault();

					new giniHealth.popup.open({target: _popup.target.getAttribute('href')});
				});
			}

			if(opt.target) _popup.init();
		},
		open: function(opt) {
			var _popup = this;

			_popup.animating = false;

			_popup.init = function() {
				_popup.target = document.createElement('div');
				$(_popup.target).addClass('com_ginihealth-popup');

				_popup.cover = document.createElement('div');
				$(_popup.cover).addClass('com_ginihealth-popup-cover');
				_popup.target.appendChild(_popup.cover);

				_popup.main = document.createElement('div');
				$(_popup.main).addClass('com_ginihealth-popup-main');
				_popup.target.appendChild(_popup.main);

				_popup.center = document.createElement('div');
				$(_popup.center).addClass('com_ginihealth-popup-center');
				_popup.main.appendChild(_popup.center);

				var _content = document.querySelector(opt.target);
				if(_content) {
					_popup.parent = _content.parentNode;
					_popup.content = _content;
					_popup.center.appendChild(_popup.content);

					var _customClass = _content.getAttribute('custom-class');
					if(_customClass) $(_popup.target).addClass(_customClass);
				} else {
					$(_popup.center).addClass('d-flex align-items-center justify-content-center');
					$(_popup.center).append(
						'<div class="popup-content">' +
							'<div class="container width-6">' +
								'<div class="po-co-box section-par text-center">' +
									'<h3>No popup found!</h3>' +
								'</div>' +
							'</div>' +
						'</div>'
					);
				}

				_popup.btn = {};
				_popup.btn.close = document.createElement('a');
				$(_popup.btn.close).addClass('com_ginihealth-popup-btn btn-close');
				_popup.btn.close.setAttribute('href', '#');
				_popup.btn.close.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 50 50"><path d="M 7.71875 6.28125 L 6.28125 7.71875 L 23.5625 25 L 6.28125 42.28125 L 7.71875 43.71875 L 25 26.4375 L 42.28125 43.71875 L 43.71875 42.28125 L 26.4375 25 L 43.71875 7.71875 L 42.28125 6.28125 L 25 23.5625 Z"/></svg>';
				_popup.center.appendChild(_popup.btn.close);

				_popup.show();

				$(_popup.btn.close).click(function(e) {
					e.preventDefault();

					if(!_popup.animating) _popup.hide();
				});

				_popup.target.addEventListener('close', function() {
					_popup.hide();
				});

				document.addEventListener('keydown', function(event) {
					if (event.key === 'Escape' || event.keyCode === 27 || event.which === 27) {
						if(_popup) _popup.hide();
					}
				});
			}

			_popup.show = function() {
				if(_popup.animating) return;
				_popup.animating = true;

				document.body.appendChild(_popup.target);
				$('body').addClass('overflow-hidden');

				$(_popup.target).addClass('popup-ready');

				setTimeout(function() {
					_popup.animating = false;
				}, 300);
			}

			_popup.hide = function() {
				if(_popup.animating) return;
				_popup.animating = true;
			
				$(_popup.target).addClass('popup-hiding');
				$('body').removeClass('overflow-hidden');

				setTimeout(function() {
					if(_popup.parent) _popup.parent.appendChild(_popup.content);
					_popup.target.remove();

					_popup.animating = false;
					_popup = null;
				}, 300);
			}

			if(opt) _popup.init();
		},
		close: function() {
			var _popup = document.querySelector('.com_ginihealth-popup');
			if(_popup) {
				giniHealth.dispatchEvent(_popup, 'close');
			} else {
				console.log('no popup found!');
			}
		}
	},
	calendar: {
		closeDates: ''
	},
	dateTimePicker: function(opt) {
		var _dt = {
			target: '',
			input: '',
			title: '',
			box: '',
			date: {
				container: '',
				current: moment().format('MM/DD/YYYY HH:mm'),
				min: 0,
				max: 10,
				selected: ''
			},
			time: {
				container: '',
				current: '',
				open: '07:00',
				close: '21:00',
				interval: 30,
				selection: [],
				selected: ''
			},
			options: '',
			firstLoad: true,
			init: function() {
				_dt.target = opt.target;
				_dt.input = _dt.target.querySelector('input');
				_dt.input.setAttribute('readonly', true);

				// if(giniHealth.debug) {
				// 	// if custom time
				// 	console.log( giniHealth.getUrlVars().time );
				// 	if(giniHealth.getUrlVars().time) {
				// 		_dt.date.current = moment(_dt.date.current, 'MM/DD/YYYY HH:mm').format('MM/DD/YYYY') + ' ' + giniHealth.getUrlVars().time;
				// 	}
				// }

				// for title
				_dt.title = document.createElement('div');
				$(_dt.title).addClass('com_datepicker-label bp-ab');
				if(_dt.input.value.length != 0) {
					$(_dt.title).html(_dt.input.value);
				} else {
					$(_dt.title).html(_dt.input.placeholder);
				}
				$(_dt.target).append(_dt.title);

				// for dd/box
				_dt.box = document.createElement('div');
				$(_dt.box).addClass('com_datepicker-box bp-ab');
				$(_dt.target).append(_dt.box);

				// get the data
				_dt.getVars();

				// create time array for <select>
				// _dt.create.timeRange();

				// create date/time containers inside dd
				_dt.create.containers();

				// show/hide the calendar dd
				$(_dt.input).click(function() {
					_dt.show();
				});
				window.addEventListener('click', function(e){   
					if (!(_dt.target.contains(e.target))){
						if(!$(e.target).hasClass('ui-corner-all')) {
							_dt.hide();
						}
					}
				});
			},
			getVars: function() {
				// custom attr
				if(opt.date) {
					if(opt.date.min) _dt.date.min = opt.date.min;
					if(opt.date.max) _dt.date.max = opt.date.max;
				}
				if(opt.time) {
					if(opt.time.open) _dt.time.open = opt.time.open;
					if(opt.time.close) _dt.time.close = opt.time.close;
					if(opt.time.interval) _dt.time.interval = opt.time.interval;
					if(opt.time.selection) _dt.time.selection = opt.time.selection;
				}

				// get current date and time
				_dt.date.current = moment().format('MM/DD/YYYY');
				_dt.time.current = moment().format('HH:mm');
				
				// if using dummy date and time
				// if(giniHealth.debug) {
				// 	var _urlVars = giniHealth.getUrlVars();
				// 	if(_urlVars) {
				// 		if(_urlVars.date) {
				// 			_dt.date.current = _urlVars.date;
				// 			_dt.date.min = new Date(moment(_dt.date.current, 'YYYY-MM-DD'));
				// 			_dt.date.max = new Date(moment(_dt.date.current, 'YYYY-MM-DD').add(_dt.date.max, 'day').format('YYYY-MM-DD'))
				// 		}

				// 		if(_urlVars.time) {
				// 			_dt.time.current = _urlVars.time;
				// 		}
				// 	}
				// }

				// if current time is beyond closing hours, least availble date will fall tomorrow or more
				// if(moment(_dt.time.current, 'HH:mm').isAfter( moment('18.00', 'HH:mm'))) _dt.date.min = _dt.date.min + 1;
				// _dt.date.selected = moment( moment(_dt.date.current, 'MM/DD/YYYY').add(_dt.date.min, 'day') ).format('MM/DD/YYYY');
			},
			create: {
				timeRange: function() {
					var _curTime = moment(_dt.time.open, 'HH:mm');

					// push the opening time
					_dt.time.selection.push(_curTime.format('HH:mm'));

					// increment the available time
					var _addTime = function() {
						if(_curTime.isBefore(moment(_dt.time.close, 'HH:mm'))) {
							var _newTime = _curTime.add(_dt.time.interval, 'minute').format('HH:mm');
							_dt.time.selection.push(_newTime);

							_addTime();
						}
					}
					_addTime();
				},
				containers: function() {
					// create calendar container
					_dt.date.container = document.createElement('div');
					$(_dt.date.container).addClass('com_dt-picker-calendar');
					_dt.box.appendChild(_dt.date.container);

					// create time container
					_dt.time.container = document.createElement('div');
					$(_dt.time.container).addClass('com_dt-picker-time');
					_dt.box.appendChild(_dt.time.container);

					// create close btn
					var _btnContainer = document.createElement('div');
					$(_btnContainer).addClass('text-center')

					var _closeBtn = document.createElement('button');
					$(_closeBtn).addClass('button button-primary button-small').html('DONE');
					_btnContainer.appendChild(_closeBtn);

					_dt.box.appendChild(_btnContainer);

					$(_closeBtn).click(function(e) {
						e.preventDefault();

						_dt.hide();
					});
				},
				calendar: function() {
					// set minDate
					let _mindate = _dt.date.min;

					// set maxDate
					let _maxDate = _dt.date.max;

					// if beyond closing time, least date to pick is tomorrow or more
					if(moment(_dt.date.current, 'MM/DD/YYYY').isSame(moment(), 'day') && moment().set({hours: _dt.time.close.split(':')[0], minutes: _dt.time.close.split(':')[1]}).isBefore()) _mindate = _mindate + 1;

					// don't reduce the maxDate if minDate is more than 1
					if(_dt.date.min >= 0) {
						_maxDate = _dt.date.max + (_mindate - 1);
					}

					_dt.options = {
						monthNames: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
						dayNamesMin: [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
						firstDay: 0,
						minDate: _mindate,
						maxDate: _maxDate,
						beforeShowDay: function(date) {
                            const _date = moment(date, 'ddd MMM DD YYYY').format('MM/DD/YYYY');
                            const _day = moment(date, 'ddd MMM DD YYYY').format('ddd');

                            const _class = 'error-message-' + moment(date, 'ddd MMM DD YYYY').format('YYYYMMDD');

                            if($.inArray(_day, giniHealth.calendar.closeDates[0])  != -1 ){
                                return [false, "", "Unavailable"];
                            }else if( $.inArray(_date, giniHealth.calendar.closeDates) != -1) {
                                return [false, _class];
                            } else {
                                return [true, ""];
                            }
						}
					}

					$(_dt.date.container).datepicker(_dt.options).on('change', function() {
						var _selected = moment(this.value, 'MM/DD/YYYY').format('MM/DD/YYYY');

						// get the selected date
						if(_selected != _dt.date.selected) {
							_dt.date.selected = _selected;

							// update the list of time again
							_dt.create.timeBox();
						}
					});
				},
				timeBox: function() {
					// remove the current select time box
					var _selectBox = _dt.time.container.querySelector('.com_select-box');
					if(_selectBox) {
						$(_selectBox).remove();
					}

					// create new select time box
					_selectBox = document.createElement('div');
					$(_selectBox).addClass('com_select-box');

					// create list of time
					var _select = document.createElement('select');

					for (var i = 0; i < _dt.time.selection.length; i++) {
						// create option
						var _option = document.createElement('option');
						_option.value = _dt.time.selection[i];
						// _option.innerHTML = moment(_dt.time.selection[i], 'HH:mm').format('hh:mm A') + ' - ' + moment(_dt.time.selection[i], 'HH:mm').add(1, 'hours').format('hh:mm A');
						_option.innerHTML = moment(_dt.time.selection[i], 'HH:mm').format('hh:mm A');

						// if this option has the same value in previous <select>, make this selected
						if(_dt.time.selection[i] == _dt.time.selected) {
							_option.setAttribute('selected', '');
						}

						// if selected date is same as current date
						if(moment(_dt.date.selected, 'MM/DD/YYYY').isSameOrBefore(moment(_dt.date.current, 'MM/DD/YYYY'))) {
							// only include the time ahead of current time
							if(moment( moment(_dt.time.current, 'HH:mm').add( (_dt.time.interval * 1) - 1, 'minute') ).isBefore( moment(_dt.time.selection[i], 'HH:mm') )) {
								_select.appendChild(_option);
							}
						} else {
						// if current day is not selected
							_select.appendChild(_option);
						}
					}

					_selectBox.appendChild(_select);

					// append select time box
					_dt.time.container.appendChild(_selectBox);

					// run the select box
					var _options = _select.querySelectorAll('option');
					if(_options.length != 0) {
						giniHealth.selectBox({target: _selectBox});

						_dt.time.selected = _select.options[_select.options.selectedIndex].value;
						_dt.update();

						_select.addEventListener('change', function() {
							_dt.time.selected = _select.options[_select.options.selectedIndex].value;
							_dt.update();
						});
					} else {
						$(_selectBox).remove();
					}
				}
			},
			update: function(state) {
				const _value = moment(_dt.date.selected, 'MM-DD-YYYY').format('DD MMM YYYY') + ' ' + moment(_dt.time.selected, 'HH:mm').format('hh:mm A');

				_dt.input.value = _value;
				$(_dt.title).html(_value);

				giniHealth.dispatchEvent(_dt.input, 'change');
			},
			show: function() {
				if(_dt.firstLoad) {
					// generate the calendar
					_dt.create.calendar();

					// generate time selector
					// _dt.create.timeBox();

					_dt.firstLoad = false;
				}

				$(_dt.box).show();
			},
			hide: function() {
				$(_dt.box).hide();
			}
		}
		_dt.init();
	},
	selectBox: function(opt) {
		var _select = {
			target: '',
			select: '',
			title: '',
			list: '',
			options: '',
			active: 0,	
			init: function() {
				_select.target = opt.target;

				// hide the <select>
				_select.select = _select.target.querySelector('select');
				_select.select.style.display = 'none';
				_select.options = _select.select.querySelectorAll('option');

				// create title
				_select.title = document.createElement('div');
				_select.title.classList.add('com_select-box-title');
				_select.target.appendChild(_select.title);

				// create list
				_select.list = document.createElement('div');
				_select.list.classList.add('com_select-box-list');

				var _ul = document.createElement('ul'),
				_firstSelected = 0;
				for (var i = 0; i < _select.options.length; i++) {
					var _thisOption = _select.options[i],
					_li = document.createElement('li'),
					_selected = _thisOption.getAttribute('selected');

					_li.innerHTML = _thisOption.innerHTML;

					_ul.appendChild(_li);

					// for li clicks
					_select.interaction(_li, i);

					// get the selected option
					if(_selected != null) _firstSelected = i;
				}
				_select.list.appendChild(_ul);
				_select.target.appendChild(_select.list);

				// for title click
				_select.title.addEventListener('click', function(e) {
					e.preventDefault();

					if(_select.options.length != 1) {
						if(_select.target.classList.contains('com_select-box-active')) {
							_select.hide();
						} else {
							_select.show();
						}
					}
				});

				// when clicking outside the select
				window.addEventListener('click', function(e){   
					if (!(_select.target.contains(e.target))) _select.hide();
				});

				// <select> event
				_select.select.addEventListener('change', function() {
					_select.update(_select.select.selectedIndex, 'self');
				});

				_select.select.addEventListener('update', function() {
					_select.update(_select.select.selectedIndex, 'self');
				});

				// select the first entry
				_select.update(_firstSelected);

				// _select.show();
			},
			update: function(number, state) {
				_select.active = number;

				// active the current list
				var _list = _select.list.querySelectorAll('li');
				for (var i = 0; i < _list.length; i++) {
					if (i == number) {
						_list[i].classList.add('active');
					} else {
						_list[i].classList.remove('active');
					}
				}

				// change the title text
				_select.title.innerHTML = _select.options[_select.active].innerHTML;

				// if value are default
				var _value = _select.options[_select.active].value; 
				if(_value == 'default') {
					_select.target.classList.add('com_select-box-default');
				} else {
					_select.target.classList.remove('com_select-box-default');
				}

				// trigger <select>
				if(state != 'self') {
					_select.select.selectedIndex = number;
					giniHealth.dispatchEvent(_select.select, 'change');
				}
			},
			show: function() {
				_select.target.classList.add('com_select-box-active');
			},
			hide: function() {
				_select.target.classList.remove('com_select-box-active');
			},
			interaction: function(target, index) {
				target.addEventListener('click', function(e) {
					e.preventDefault();

					_select.hide();
					if(_select.active != index) _select.update(index);
				});
			}
		}

		if(!opt.target.querySelector('.com_select-box-title')) _select.init();
	}
}

giniHealth.init();