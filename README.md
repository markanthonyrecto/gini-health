## Usage Guide

### Requirements
- Node.js (`v14.18.0` or later)
- npm (`v8.12.1` or later)

### Instructions
1. Clone the repository: `git clone https://gitlab.com/markanthonyrecto/gini-health`
2. Install dependencies: `npm install`
3. Run the development server: `gulp watch`

### Troubleshooting
- If you encounter any issues, ensure that you're using the recommended versions of Node.js and npm.
- If problems persist, try deleting the `/node_modules` folder and the `package-lock.json` file, then run `npm install` again.

### Output
- Output files such as HTML, CSS, JavaScript, and images can be found in the `/public` folder.
- Raw HTML files can be found in the `/source` directory.